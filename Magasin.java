public class Magasin {
   private String nom;
   private boolean ouvertLundi, ouvertDimanche ;

   public Magasin(String nom, boolean lundi, boolean dimanche) {
       this.nom = nom;
       this.ouvertLundi = lundi;
       this.ouvertDimanche = dimanche;
   }
   public String getNom() {return this.nom;}
   public boolean getLundi() {return this.ouvertLundi;}
   public boolean getDimanche() {return this.ouvertDimanche;}

   @Override
   public String toString() {
   String res = "";
   res+= "Le magasin " +this.nom;
   if (this.ouvertLundi)
      res+= "est ouvert le lundi";
   else
      res+= "est fermé le lundi";
   if (this.ouvertDimanche)
      res+= "est ouvert le dimanche";
   else
      res+= "est fermé le dimanche";
   return res;
   }
}
